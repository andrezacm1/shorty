require 'sinatra'
require 'sequel'

DB = Sequel.connect(ENV['DATABASE_URL'])

Sequel.default_timezone = :utc

%w[models routes errors].each do |dir|
  Dir.glob("./lib/#{dir}/*.rb", &method(:require))
end
