before do
  content_type 'application/json'
end

post '/shorten' do
  params = JSON.parse(request.body.read)

  short_url = ShortURL.new(
    url: params['url'],
    shortcode: params['shortcode'] || ShortURL.generate_shortcode,
    shortened_at: Time.now
  )

  if short_url.save
    status(201)
    body({ shortcode: short_url.shortcode }.to_json)
  else
    error = ErrorMapper.new(short_url).error
    halt(error.http_code, error.message)
  end
end

get '/:shortcode' do |shortcode|
  short_url = ShortURL.first(shortcode: shortcode)

  if short_url.nil?
    error = ErrorMapper.new(short_url).error
    halt(error.http_code, error.message)
  end

  short_url.mark_as_visited

  response.headers['Location'] = short_url.url
  status(302)
end

get '/:shortcode/stats' do |shortcode|
  short_url = ShortURL.first(shortcode: shortcode)

  if short_url.nil?
    error = ErrorMapper.new(short_url).error
    halt(error.http_code, error.message)
  end

  status(200)
  body(short_url.stats.to_json)
end
