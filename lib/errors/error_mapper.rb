class ErrorMapper
  def initialize(short_url)
    @short_url = short_url
  end

  def error
    return not_found if short_url.nil?
    return bad_request if short_url.errors[:url]
    return conflict if short_url.errors[:shortcode].include? 'is already taken'
    return unprocessable_entity if short_url.errors[:shortcode].include? 'is invalid'
  end

  private

  attr_reader :short_url

  def not_found
    Error.new(404, 'The shortcode cannot be found in the system.')
  end

  def bad_request
    Error.new(400, 'URL is not present.')
  end

  def conflict
    Error.new(
      409,
      'The desired shortcode is already in use. ' \
      'Shortcodes are case-sensitive.'
    )
  end

  def unprocessable_entity
    Error.new(
      422,
      'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$.'
    )
  end

  class Error
    def initialize(http_code, message)
      @http_code = http_code
      @message = message
    end

    attr_reader :http_code, :message
  end
end
