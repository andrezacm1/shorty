class ShortURL < Sequel::Model
  plugin :validation_helpers
  self.raise_on_save_failure = false

  SHORTCODE_CHARS = [*('a'..'z'), *('A'..'Z'), *('0'..'9'), '_'].freeze

  def validate
    super
    validates_presence([:url, :shortcode, :shortened_at])
    validates_format(/^[0-9a-zA-Z_]{4,}$/, :shortcode)
    validates_unique(:shortcode)
  end

  def self.generate_shortcode
    6.times.map { SHORTCODE_CHARS[rand(SHORTCODE_CHARS.length)] }.join
  end

  def mark_as_visited
    self.last_seen_at = Time.now
    self.redirect_count += 1
    self.save
  end

  def stats
    Hash.new.tap do |stats|
      stats[:startDate] = parse_to_iso8601(self.shortened_at)
      stats[:redirectCount] = self.redirect_count

      if self.last_seen_at && self.redirect_count > 0
        stats[:lastSeenDate] = parse_to_iso8601(self.last_seen_at)
      end
    end
  end

  private

  def parse_to_iso8601(time)
    time.getutc.iso8601
  end
end
