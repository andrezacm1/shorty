require 'spec_helper'

describe ErrorMapper do
  describe '#error' do
    subject { ErrorMapper.new(short_url).error }

    context 'when short_url is nil' do
      let(:short_url) { nil }

      it 'returns error code 404' do
        expect(subject.http_code).to eq(404)
      end

      it 'returns not found error message' do
        expect(subject.message).to eq(
          'The shortcode cannot be found in the system.'
        )
      end
    end

    context 'when short_url does not have url' do
      before { short_url.valid? }
      let(:short_url) { ShortURL.new(shortcode: 'short') }

      it 'returns error code 400' do
        expect(subject.http_code).to eq(400)
      end

      it 'returns bad request error message' do
        expect(subject.message).to eq(
          'URL is not present.'
        )
      end
    end

    context 'when short_url has a shortcode already taken' do
      before do
        ShortURL.create(
          url: 'url.com', shortcode: 'short', shortened_at: Time.now
        )

        short_url.valid?
      end

      let(:short_url) { ShortURL.new(url: 'url.com', shortcode: 'short') }

      it 'returns error code 409' do
        expect(subject.http_code).to eq(409)
      end

      it 'returns conflict error message' do
        expect(subject.message).to eq(
          'The desired shortcode is already in use. ' \
          'Shortcodes are case-sensitive.'
        )
      end
    end

    context 'when short_url has a invalid shortcode' do
      before { short_url.valid? }
      let(:short_url) do
        ShortURL.new(url: 'url.com', shortcode: 'invalid_shortcode_$')
      end

      it 'returns error code 422' do
        expect(subject.http_code).to eq(422)
      end

      it 'returns unprocessable entity error message' do
        expect(subject.message).to eq(
          'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$.'
        )
      end
    end
  end
end
