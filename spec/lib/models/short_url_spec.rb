require 'spec_helper'

describe ShortURL do
  describe 'validations' do
    context 'required attrs' do
      subject { ShortURL.new }

      %i[url shortcode shortened_at].each do |required_attr|
        it "validates presence of #{required_attr}" do
          subject.valid?
          expect(subject.errors[required_attr]).to include('is not present')
        end
      end
    end

    context 'attrs format' do
      subject { ShortURL.new(shortcode: 'invalid_format_&') }

      it 'validates valid format of shortcode' do
        subject.valid?
        expect(subject.errors[:shortcode]).to include('is invalid')
      end
    end

    context 'attrs uniqueness' do
      subject { ShortURL.new(shortcode: 'short') }

      before do
        ShortURL.create(
          url: 'url.com', shortcode: 'short', shortened_at: Time.now
        )
      end

      after { ShortURL.dataset.destroy }

      it 'validates uniqueness of shortcode' do
        subject.valid?
        expect(subject.errors[:shortcode]).to include('is already taken')
      end
    end
  end

  describe '.generate_shortcode' do
    subject { ShortURL.generate_shortcode }

    it 'generates valid shortcode' do
      expect(subject).to match(/^[0-9a-zA-Z_]{6}$/)
    end
  end

  describe '#mark_as_visited' do
    let(:short_url) do
      ShortURL.create(
        url: 'url.com', shortcode: 'short', shortened_at: Time.now
      )
    end

    it 'updates last_seen_at' do
      expect { short_url.mark_as_visited }.to change(short_url, :last_seen_at).from(nil)
    end

    it 'add 1 to redirect_count' do
      expect { short_url.mark_as_visited }.to change(
        short_url, :redirect_count
      ).from(0).to(1)
    end
  end

  describe '#stats' do
    let(:short_url) do
      ShortURL.create(
        url: 'url.com', shortcode: 'short', shortened_at: Time.now
      )
    end

    context 'when resource has not been visited' do
      it 'parses stats attrs without last_seen_at' do
        expect(short_url.stats).to eq(
          {
            startDate: short_url.shortened_at.getutc.iso8601,
            redirectCount: 0
          }
        )
      end
    end

    context 'when resource has been visited' do
      it 'parses stats attrs' do
        short_url.mark_as_visited

        expect(short_url.stats).to eq(
          {
            startDate: short_url.shortened_at.getutc.iso8601,
            redirectCount: 1,
            lastSeenDate: short_url.last_seen_at.getutc.iso8601,
          }
        )
      end
    end
  end
end
