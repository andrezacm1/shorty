require 'spec_helper'

describe 'POST /shorten' do
  context 'when all requirements are met' do
    it 'returns status 201' do
      post('/shorten', { url: 'url.com', shortcode: 'short' }.to_json)

      expect(last_response).to be_created
    end

    it 'returns shortcode created' do
      post('/shorten', { url: 'url.com', shortcode: 'short' }.to_json)

      response = JSON.parse(last_response.body)

      expect(response).to eq({ 'shortcode' => 'short' })
    end
  end

  context 'when url param is not present' do
    it 'returns status 400' do
      post('/shorten', { shortcode: 'short' }.to_json)

      expect(last_response).to be_bad_request
    end

    it 'returns error message' do
      post('/shorten', { shortcode: 'short' }.to_json)

      expect(last_response.body).to eq('URL is not present.')
    end
  end

  context 'when shortcode param is not present' do
    it 'generates a shortcode' do
      expect(ShortURL).to receive(:generate_shortcode).and_call_original

      post('/shorten', { url: 'url.com' }.to_json)
    end
  end

  context 'when desired shortcode has already been taken' do
    before do
      ShortURL.create(
        url: 'url.com', shortcode: 'short', shortened_at: Time.now
      )
    end

    it 'returns status 409' do
      post('/shorten', { url: 'url.com', shortcode: 'short' }.to_json)

      expect(last_response.status).to eq(409)
    end

    it 'returns error message' do
      post('/shorten', { url: 'url.com', shortcode: 'short' }.to_json)

      expect(last_response.body).to eq(
        'The desired shortcode is already in use. ' \
        'Shortcodes are case-sensitive.'
      )
    end
  end

  context 'when shortcode has invalid format' do
    it 'returns status 422' do
      post('/shorten', { url: 'url.com', shortcode: 'invalid_#' }.to_json)

      expect(last_response).to be_unprocessable
    end

    it 'returns error message' do
      post('/shorten', { url: 'url.com', shortcode: 'invalid_#' }.to_json)

      expect(last_response.body).to eq(
        'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$.'
      )
    end
  end
end
