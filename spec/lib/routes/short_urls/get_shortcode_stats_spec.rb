require 'spec_helper'

describe 'GET /:shortcode/stats' do
  before do
    ShortURL.create(
      url: 'url.com', shortcode: 'short', shortened_at: Time.utc(2018, 10, 07)
    )
  end

  context 'when shortcode is in the system' do
    it 'returns status 200' do
      get('/short/stats')

      expect(last_response).to be_ok
    end

    it 'returns parsed stats' do
      get('/short/stats')

      response = JSON.parse(last_response.body)

      expect(response).to eq(
        {
          'startDate' => '2018-10-07T00:00:00Z',
          'redirectCount' => 0
        }
      )
    end
  end

  context 'when shortcode cannot be found' do
    it 'returns status 404' do
      get('/shortcode_not_existent/stats')

      expect(last_response).to be_not_found
    end

    it 'returns error message' do
      get('/shortcode_not_existent/stats')

      expect(last_response.body).to eq(
        'The shortcode cannot be found in the system.'
      )
    end
  end
end
