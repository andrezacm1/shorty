require 'spec_helper'

describe 'GET /:shortcode' do
  before do
    ShortURL.create(
      url: 'url.com', shortcode: 'short', shortened_at: Time.now
    )
  end

  context 'when shortcode is in the system' do
    it 'calls mark_as_visited method for found resource' do
      expect_any_instance_of(ShortURL).to receive(:mark_as_visited)

      get('/short')
    end
    
    it 'returns status 302' do
      get('/short')

      expect(last_response.status).to eq(302)
    end

    it 'returns url in location header' do
      get('/short')

      location = last_response.headers['Location']

      expect(location).to eq('url.com')
    end
  end

  context 'when shortcode cannot be found' do
    it 'returns status 404' do
      get('/shortcode_not_existent')

      expect(last_response).to be_not_found
    end

    it 'returns error message' do
      get('/shortcode_not_existent')

      expect(last_response.body).to eq(
        'The shortcode cannot be found in the system.'
      )
    end
  end
end
