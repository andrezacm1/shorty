Sequel.migration do
  up do
    create_table(:short_urls) do
      primary_key :id
      String :url, null: false
      String :shortcode, null: false, unique: true
      DateTime :shortened_at, null: false
      DateTime :last_seen_at
      Integer :redirect_count, default: 0
    end
  end

  down do
    drop_table(:short_urls)
  end
end
